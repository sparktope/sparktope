An assortment of simple examples, driven by a Makefile, can be found
in `demo`. Two more substantial examples due to David Avis can be
found in `makespan` and `maxmatching`. The latter two have their own
documentation.

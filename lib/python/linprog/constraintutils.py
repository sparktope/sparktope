from linprog.LPVariable import LPVariable
from linprog.intutils import *

def S(i,t):
    return LPVariable('S','array',[i,t])

def K(var,t):
    return LPVariable('_K','array',[var.name,t])

def index_control(var,index,t):
    return LPVariable("_{var:s}c".format(var=var),
                      'array',
                      [index, t])

# temporary variables for operators.
def otemp(j,t):
    return LPVariable('_ot','array',[j,t])


# temporary variables for array references
def atemp(var,count,j,t):
    return LPVariable('_{var:s}{count:d}'. \
                      format(var=var,count=count),
                      'array', [j,t])

def M(count,j,t):
    return atemp('M',count,j,t)

def N(count,j,t):
    return atemp('N',count,j,t)

# convenience function for array references
def T(sign,index, bit_index, index_bit):
    if select_bit(index,bit_index) == 0:
        return [(sign,index_bit)]
    else:
        return [LPVariable(sign,'int',[]),
                (-1*sign, index_bit)]

def negsense(opstr):
    if opstr==' <= ':
        return ' >= '
    elif opstr==' >= ':
        return ' <= '
    return opstr

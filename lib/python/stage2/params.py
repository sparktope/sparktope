import ast
import operator as op
from linprog.intutils import integerish

class ParamSet:
    # supported operators
    operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                 ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
                 ast.Mod: op.mod,
                 ast.USub: op.neg}


    def __init__(self,filelist):
        self.parameters = {}

        for paramfile in filelist:
            with open (paramfile) as file:
                for line in file:
                    (key,val) = line.rstrip().split("=")
                    parts=key.split('.')
                    table=self.parameters
                    key=parts.pop(0)
                    while len(parts)>0:
                        if not table.get(key):
                            table[key]={}
                        table=table[key]
                        key=parts.pop(0)
                        
                    table[key] = int(val)

        if not 'start_line' in self.parameters:
            self.parameters['start_line']=None

    def __str__(self):
        return str(self.parameters)
            
    # support table[key] notation
    def __getitem__(self, key):
        return self.parameters[str(key)]

    def __contains__(self, key):
        return self.parameters.__contains__(str(key))

    def __len__(self):
        return self.parameters.__len__()
    def __iter__(self):
        return self.parameters.__iter__()

    def __iterkeys__(self):
        return self.parameters.__iterkeys__()

    def iteritems(self):
        return self.parameters.iteritems()

    def itervalues(self):
        return self.parameters.itervalues()


    # by J.F. Sebastian, on Stack Overflow.
    def eval_expr(self,expr):
        if integerish(expr):
            return int(expr)
        else:
            return int(self.eval_(ast.parse(expr, mode='eval').body))

    def eval_(self,node):
        if isinstance(node, ast.Num): # <number>
            return node.n
        elif isinstance(node,ast.Name): # <ident>
            return self.parameters[node.id]
        elif isinstance(node, ast.BinOp): # <left> <operator> <right>
            return ParamSet.operators[type(node.op)](self.eval_(node.left), self.eval_(node.right))
        elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
            return ParamSet.operators[type(node.op)](self.eval_(node.operand))
        else:
            raise TypeError(node)

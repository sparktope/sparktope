@ phase start init
@ input matrix a n n	# L2
@ output bool w	# L3
@ var triangular A n n	# L5
@ var array odd n	# L6
@ var array marked n	# L7
@ var array F n	# L8
@ var array shrunk n	# L9
@ var int_array match n	# L10
@ var int_array parent n	# L11
@ var bool x	# L12
@ var bool y	# L12
@ var bool z	# L12
@ var bool progress	# L12
@ var bool swap	# L12
@ var bool edge	# L12
@ var bool doneW	# L12
@ var bool doneV	# L12
@ var bool tip	# L12
@ var int i	# L13
@ var int j	# L13
@ var int k	# L13
@ var int V	# L13
@ var int W	# L13
@ var int X	# L13
@ var int Y	# L13
@ var int row	# L13
@ var int col	# L13
. matrix_init A 0	# L15
. array_init shrunk 0	# L16
. set i copyw 0	# L17
. set _sentinel3 copyw $n-1$	# L17
for3 row_set match i i	# L18
. set _test3 eqw i _sentinel3	# L17
. if _test3 done3	# L17
. set i incw i	# L17
. goto for3	# L17
done3 set i copyw 0	# L21
. set _sentinel5 copyw $n-2$	# L21
for5 set j incw i	# L22
. set _sentinel6 copyw $n-1$	# L22
for6 set  _temp8 matrix_ref a i j	# L23
. matrix_set A i j _temp8	# L23
. set  _temp10 matrix_ref a j i	# L24
. set guard9 copy _temp10	# L24
. unless guard9 else9	# L24
9 row_set match i j	# L25
. row_set match j i	# L26
else9 nop	# L24
. set _test6 eqw j _sentinel6	# L22
. if _test6 done6	# L22
. set j incw j	# L22
. goto for6	# L22
done6 nop	# L22
. set _test5 eqw i _sentinel5	# L21
. if _test5 done5	# L21
. set i incw i	# L21
. goto for5	# L21
done5 set progress copy 1	# L30
@ phase end init
@ phase start main
14 set _test15 copy progress	# L35
. unless _test15 done15	# L35
16 array_init odd 0	# L36
. array_init marked 0	# L36
. set i copyw 0	# L37
. set _sentinel18 copyw $n-1$	# L37
for18 set  _temp20 array_ref shrunk i	# L38
. set guard19 copy _temp20	# L38
. unless guard19 else19	# L38
19 array_set marked i 1	# L38
. goto done19	# L38
else19 row_set parent i i	# L40
. set  _temp24 row_ref match i	# L41
. set _temp25 eqw i _temp24	# L41
. array_set F i _temp25	# L41
done19 nop	# L38
. set _test18 eqw i _sentinel18	# L37
. if _test18 done18	# L37
. set i incw i	# L37
. goto for18	# L37
done18 set progress copy 0	# L44
. set V copyw 0	# L44
. set doneV copy 0	# L44
while29 set  _temp30 not progress	# L45
. set  _temp31 not doneV	# L45
. set _test29 and _temp30 _temp31	# L45
. unless _test29 done29	# L45
30 set  _temp33 array_ref marked V	# L46
. set  _temp34 not _temp33	# L46
. set  _temp35 array_ref odd V	# L46
. set  _temp36 not _temp35	# L46
. set x and _temp34 _temp36	# L46
. set  _temp38 array_ref F V	# L47
. set guard37 and x _temp38	# L47
. unless guard37 else37	# L47
37 array_set marked V 1	# L48
. set W copyw 0	# L48
. set doneW copy 0	# L48
while42 set  _temp43 not progress	# L49
. set  _temp44 not doneW	# L49
. set _test42 and _temp43 _temp44	# L49
. unless _test42 done42	# L49
43 set _temp48 eqw V W	# L50
. set _temp46 not _temp48	# L50
. set  _temp49 array_ref shrunk W	# L50
. set _temp47 not _temp49	# L50
. set guard45 and _temp46 _temp47	# L50
. unless guard45 else45	# L50
45 set guard50 ltw V W	# L51
. unless guard50 else50	# L51
50 set  _temp52 matrix_ref A V W	# L51
. set edge copy _temp52	# L51
. goto done50	# L51
else50 set  _temp54 matrix_ref A W V	# L52
. set edge copy _temp54	# L52
done50 set  _temp56 array_ref odd W	# L53
. set  _temp57 not _temp56	# L53
. set guard55 and edge _temp57	# L53
. unless guard55 else55	# L53
55 set  _temp59 array_ref F W	# L54
. set guard58 not _temp59	# L54
. unless guard58 else58	# L54
58 set  _temp61 row_ref match W	# L55
. set X copyw _temp61	# L55
. row_set parent W V	# L56
. row_set parent X W	# L57
. array_set F W 1	# L58
. array_set F X 1	# L58
. array_set odd W 1	# L59
. array_set odd X 0	# L59
. goto done58	# L54
else58 set i copyw V	# L61
while69 set  _temp71 row_ref parent i	# L62
. set _temp70 eqw i _temp71	# L62
. set _test69 not _temp70	# L62
. unless _test69 done69	# L62
70 set  _temp73 row_ref parent i	# L62
. set i copyw _temp73	# L62
. goto while69	# L62
done69 set k copyw W	# L63
while75 set  _temp77 row_ref parent k	# L64
. set _temp76 eqw k _temp77	# L64
. set _test75 not _temp76	# L64
. unless _test75 done75	# L64
76 set  _temp79 row_ref parent k	# L64
. set k copyw _temp79	# L64
. goto while75	# L64
done75 set _temp81 eqw i k	# L65
. set guard80 not _temp81	# L65
. unless guard80 else80	# L65
80 return w copy 1	# L65
. goto done80	# L65
else80 set  _temp84 row_ref parent V	# L67
. set X copyw _temp84	# L67
. row_set parent V W	# L68
while86 set _temp87 eqw V X	# L69
. set _test86 not _temp87	# L69
. unless _test86 done86	# L69
87 set  _temp89 row_ref parent X	# L70
. set Y copyw _temp89	# L70
. row_set parent X V	# L71
. set V copyw X	# L72
. set X copyw Y	# L72
. goto while86	# L69
done86 set  _temp94 row_ref match W	# L74
. set V copyw _temp94	# L74
. set tip copy 0	# L74
while96 set _temp97 eqw V W	# L75
. set _test96 not _temp97	# L75
. unless _test96 done96	# L75
97 array_set shrunk V 1	# L76
. set guard99 not tip	# L77
. unless guard99 else99	# L77
99 set  _temp103 array_ref odd V	# L78
. set _temp101 not _temp103	# L78
. set  _temp105 row_ref parent V	# L78
. set  _temp106 row_ref match V	# L78
. set _temp104 eqw _temp105 _temp106	# L78
. set _temp102 not _temp104	# L78
. set guard100 and _temp101 _temp102	# L78
. unless guard100 else100	# L78
100 set tip copy 1	# L79
. set  _temp109 row_ref match V	# L80
. set guard108 eqw _temp109 V	# L80
. unless guard108 else108	# L80
108 row_set match W W	# L80
. goto done108	# L80
else108 set  _temp112 row_ref match V	# L81
. row_set match W _temp112	# L81
done108 nop	# L80
else100 nop	# L78
else99 set j copyw 0	# L84
. set swap copy 0	# L84
. set col copyw W	# L84
while116 set _test116 ltw j V	# L85
. unless _test116 done116	# L85
117 set guard117 eqw W j	# L86
. unless guard117 else117	# L86
117 set swap copy 1	# L86
. set row copyw W	# L86
. goto done117	# L86
else117 set guard120 copy swap	# L88
. unless guard120 else120	# L88
120 set col copyw j	# L88
. goto done120	# L88
else120 set row copyw j	# L88
done120 set  _temp124 matrix_ref A row col	# L89
. set  _temp125 matrix_ref A j V	# L89
. set _temp126 or _temp124 _temp125	# L89
. matrix_set A row col _temp126	# L89
done117 set j incw j	# L91
. goto while116	# L85
done116 set _temp129 eqw j $n-1$	# L93
. set _test128 not _temp129	# L93
. unless _test128 done128	# L93
129 set j incw j	# L94
. set guard131 eqw W j	# L95
. unless guard131 else131	# L95
131 set swap copy 1	# L95
. set row copyw W	# L95
. goto done131	# L95
else131 set guard134 copy swap	# L97
. unless guard134 else134	# L97
134 set col copyw j	# L97
. goto done134	# L97
else134 set row copyw j	# L97
done134 set  _temp138 matrix_ref A row col	# L98
. set  _temp139 matrix_ref A V j	# L98
. set _temp140 or _temp138 _temp139	# L98
. matrix_set A row col _temp140	# L98
done131 nop	# L95
. goto done116	# L93
done128 set  _temp142 row_ref parent V	# L101
. set V copyw _temp142	# L101
. goto while96	# L75
done96 nop	# L75
done80 set progress copy 1	# L104
done58 nop	# L54
else55 nop	# L53
else45 set guard144 eqw W $n-1$	# L108
. unless guard144 else144	# L108
144 set doneW copy 1	# L108
. goto done144	# L108
else144 set W incw W	# L109
done144 nop	# L108
. goto while42	# L49
done42 nop	# L49
else37 set guard147 eqw V $n-1$	# L112
. unless guard147 else147	# L112
147 set doneV copy 1	# L112
. goto done147	# L112
else147 set V incw V	# L113
done147 nop	# L112
. goto while29	# L45
done29 nop	# L45
. goto 14	# L35
done15 return w copy 0	# L116
@ phase end main
. nop	# L34

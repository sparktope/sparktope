#lang racket/base
(require parser-tools/lex (prefix-in : parser-tools/lex-sre))
(require ragg/support)
(provide tokenize tokenize-string)

(define (tokenize-string str)
  (let ([ip (open-input-string str)])
	(tokenize ip)))

(define (tokenize ip)
  (port-count-lines! ip)
    (define my-lexer
      (lexer-src-pos
       [(:or "const" "int" "bool" "input" "output" "return" "array"
             "matrix"
             "if" "then" "else" "endif" "phase"
             "while" "for" "do" "done" "and" "or" "xor" "not" "eq" "nop" "triangular")
        (token (string->symbol (string-upcase lexeme)) lexeme)]
       [(:or "[[" "]]" "{" "}" "(" ")" "[" "]"
	     "<-" "<" "!" "=" "!=" "++" "," "+" "@" ":" "*" "/")
	(token lexeme lexeme)]
       [(:+ numeric)
         (token 'NUMBER (string->number lexeme))]
       [(:+ alphabetic (:or alphabetic numeric "_") )
	(token 'IDENT  lexeme)]
       [(:: "#" (:* (:~ "\n")))
        (token 'COMMENT lexeme #:skip? #t)]
       [(:or whitespace ";")
        (token 'WHITESPACE lexeme #:skip? #t)]
       [#\$   (token 'MACROSTRING (list->string (get-macrostring-token input-port)))]
       [(eof) (token eof)]))
    
    (define (next-token) (my-lexer ip))
    next-token)

  (define get-macrostring-token
    (lexer
     [(:~ #\$ #\\) (cons (car (string->list lexeme))
                         (get-macrostring-token input-port))]
     [(:: #\\ #\\) (cons #\\ (get-macrostring-token input-port))]
     [(:: #\\ #\$) (cons #\$ (get-macrostring-token input-port))]
     [#\$ null]))

(module+ test
  (define (token-list lexer)
     (let ([tok (lexer)])
       (if (equal? (position-token-token tok) (token eof))
           '()
           (cons tok (token-list lexer)))))

  (define lexer (tokenize-string "
phase init
input bool x
const int p
matrix A[5,5]
triangular matrix B[5,5]
array y [$p+1$]
done
y[*] <- 1
z <- a < b
nop
"))
  (token-list lexer))


# force default target if none given on command line
docs:

LATEX ?= pdflatex
BIBTEX ?= bibtex

%.pdf: %.bib %.tex
	cd $(dir $*) && \
	$(LATEX) $(notdir $*) && \
	$(BIBTEX) $(notdir $*) && \
	$(LATEX) $(notdir $*) && \
	$(LATEX) $(notdir $*)

TEXPDFDOCS=doc/manual.pdf \
	examples/makespan/doc/ms.pdf \
	examples/maxmatching/doc/mm.pdf

docs: $(TEXPDFDOCS)

.PHONY: clean

CLEAN:= $(TEXPDFDOCS) $(TEXPDFDOCS:.pdf=.aux) $(TEXPDFDOCS:.pdf=.bbl) \
	$(TEXPDFDOCS:.pdf=.blg) $(TEXPDFDOCS:.pdf=.log)

clean:
	rm -f $(CLEAN)

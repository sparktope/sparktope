@ input array p $m$	# L1
@ var matrix x $m$ $n$	# L2
@ output bool w	# L3
@ var int i	# L4
@ var int j	# L4
@ var int T	# L4
@ var int last	# L4
@ var int proc	# L5
@ var bool single	# L5
. matrix_init x 0	# L6
. set T copyw 0	# L6
. set proc copyw $n-1$	# L6
. set i copyw 0	# L7
. set _sentinel3 copyw $m-1$	# L7
for3 set  _temp5 array_ref p i	# L8
. set guard4 copy _temp5	# L8
. unless guard4 else4	# L8
4 set proc incw proc	# L9
. set guard7 eqw proc $n$	# L10
. unless guard7 else7	# L10
7 set proc copyw 0	# L10
else7 matrix_set x i proc 1	# L11
. set guard10 eqw proc 0	# L12
. unless guard10 else10	# L12
10 set _temp12 copyw T	# L12
. set _temp13 copyw 2	# L12
. set T addw _temp12 _temp13	# L12
else10 nop	# L12
else4 nop	# L8
. set _test3 eqw i _sentinel3	# L7
. if _test3 done3	# L7
. set i incw i	# L7
. goto for3	# L7
done3 set single copy 0	# L15
. set last incw proc	# L16
. set guard16 eqw last $n$	# L17
. unless guard16 else16	# L17
16 set last copyw 0	# L18
else16 set i copyw 0	# L20
. set _sentinel18 copyw $m-1$	# L20
for18 set  _temp20 array_ref p i	# L21
. set guard19 not _temp20	# L21
. unless guard19 else19	# L21
19 set proc incw proc	# L22
. set guard22 eqw proc $n$	# L23
. unless guard22 else22	# L23
22 set guard23 copy single	# L24
. unless guard23 else23	# L24
23 set last copyw 0	# L24
else23 set proc copyw last	# L25
. set single copy 1	# L26
. set guard27 eqw last 0	# L27
. unless guard27 else27	# L27
27 set T incw T	# L27
else27 nop	# L27
else22 matrix_set x i proc 1	# L29
else19 nop	# L21
. set _test18 eqw i _sentinel18	# L20
. if _test18 done18	# L20
. set i incw i	# L20
. goto for18	# L20
done18 return w copy 0	# L32

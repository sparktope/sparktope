
ASM->GMPL compiler
==================

This directory contains the second stage compiler.


Generating the LP
-----------------

% python stage2.py hello.asm hello-2.param > hello-2.gmpl

input:

example.asm: "assembly", the low level pseudocode output from stage1.

example-1.param: instance parameters to compile for.
syntax is 

variable=value

one per line.

In general you need to specify at least a 'word_size', and 'time_bound'.

In the name hello-2.param, 2 is some unique identifier for the the
parameter set, here the word size.


Running the LP
--------------

The objective function specifying the input must be given in a
seperate .dat file.

Run the solver with 

% glpsol -m hello-2.gmpl -d hello-2-0.dat

% glpsol --exact 

For small examples, we adopt the convention of encoding the input into
the file name, after the parameter set specifier.



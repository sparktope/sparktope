from linprog.intutils import integerish
from collections import deque
import re
class StatementList:

    def __init__(self, program,file):
        self.queue = deque()
        self.program = program
        self.file = file
        self.labels = program.stmt_labels
        self.count=0
        self.active_phases = []

    def __iter__(self):
        return self

    def next(self):
        if len(self.queue)==0:

            directive=True
            while directive:
                line=self.file.next().rstrip()
                self.count += 1
                with_comment=[ p.strip() for p in line.split("#") ]
                parts = [p for p in re.split("( |[$][^$]*[$]|)", with_comment[0]) if p.strip()]
                line_label = parts.pop(0)
                directive = line_label == '@'
                if directive:
                    self.process_directive(parts)

            if line_label != '.':
                self.labels[line_label] = self.count


            # macro expansion could be inserted here
            self.queue.append(Statement(self.program,self.count,
                                        parts,self.active_phases[:]))

        return self.queue.popleft()

    def process_directive(self, parts):
        program=self.program

        parts = [ program.param_expand(p) for p in parts ]
        if parts[0]=='phase':
            if parts[1]=='start':
                self.active_phases.append(parts[2])
            elif parts[1]=='end':
                if self.active_phases[-1] != parts[2]:
                    raise Exception('phase mismatch',self.active_phases[-1],parts[2])
                self.active_phases.pop()
        elif parts[0] in ['bool', 'int']:
            program.declare('var', parts[1],parts[0])
        elif parts[0] in [ 'var', 'input', 'output']:
            if parts[1] in ['bool', 'int']:
                program.declare(parts[0],parts[2],parts[1])
            elif parts[1] == 'array':
                size = program.intparam(parts[3])

                program.declare(parts[0], parts[2],
                                'array', [size])
            elif parts[1] == 'matrix':
                program.declare(parts[0], parts[2],
                                'matrix',
                                [program.intparam(parts[3]),
                                 program.intparam(parts[4]) ])
            elif parts[1] == 'triangular':
                program.declare(parts[0], parts[2],
                                'matrix',
                                [program.intparam(parts[3]),
                                 program.intparam(parts[4]),
                                 'triangular'])
            elif parts[1] == 'int_array':
                program.declare(parts[0], parts[2],
                                'matrix',
                                [program.intparam(parts[3]),
                                 program.intparam('word_size')])


class Statement:

    def __init__(self, program, line, parts, phases):


        self.stype = None
        self.op = None
        self.args = []

        self.line = line
        self.phases=phases
        self.parse(program, [ program.param_expand(p) for p in parts ])


    def __str__(self):
        return '{0:d} {1:s} {2:s} {3:s} {4:6}'.format(self.line,
                                                      self.stype,
                                                      self.op,
                                                      self.args,
                                                      self.phases)

    def parse (self, program, parts):

        self.stype=parts[0]
        if self.stype in ['set', 'return']:
            self.op=program.ops["set_"+parts[2]]

            if self.stype == 'set':
                program.declare('var',parts[1],self.op.return_type())
                # mark it modified on the statement being created
                program.mark_modified(parts[1], self.line)

            if parts[2] in ['array_ref', 'matrix_ref']:

                program.symbols.declare_array_temp('M',
                                                   0,
                                                   parts[3])

                program.declare_index(parts[4],
                                      'row',
                                      self.line)

            if parts[2] == 'matrix_ref':
                program.symbols.declare_array_temp('N',
                                                   0,
                                                   parts[3],
                                                   True)
                program.declare_index(parts[5],
                                      'col',
                                      self.line)

            if parts[2] == 'row_ref':
                for j in range(0, program.params['word_size']):
                    program.symbols.declare_array_temp('M',
                                               j,
                                               parts[3])
                    program.declare_index(parts[4],
                                          'row',
                                          self.line)

            self.args=[parts[1]]+parts[3:]

        elif self.stype in  ['array_init', 'matrix_init']:
            program.mark_modified(parts[1], self.line)
            self.args=parts[1:]

        elif self.stype in [ 'array_set', 'matrix_set', 'row_set' ]:

            program.mark_modified(parts[1], self.line)
            program.declare_index(parts[2], 'row', self.line)

            if self.stype in ['matrix_set']:
                program.declare_index(parts[3], 'col',
                                      self.line)

            if self.stype == 'row_set':
                limit=program.params['word_size']
            else:
                limit=1

            for j in range(0,limit):
                program.symbols.declare_array_temp('M',
                                               j,
                                               parts[1])

                if self.stype in ['matrix_set']:
                    program.symbols.declare_array_temp('N',
                                                       j,
                                                       parts[1],
                                                       True)
            self.args=parts[1:]
        else:
            self.op=None
            self.args=parts[1:]

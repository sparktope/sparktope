// based on the pseudocode in https://en.wikipedia.org/wiki/Blossom_algorithm
// model for apm.spk

#include <math.h>
#include <stdio.h>
#define TRUE 1
#define FALSE 0

int A[50][50]; // Graph above the diagonal, matching below it 
int match[50];
int odd[50];
int shrunk[50];
int F[50];       // true if v in forest else false
int blossom;     // one if blossom found at the last iteration
int matched=0;   // number of matched vertices
int N;           // nodes of graph 0,1,...,N

int marked[50];
int parent[50];


void printdata();
void printpath(int w, int v); // path from w,v, to root[v]
void printblossom(int w, int v); 

int main()/*blossom algorithm derived from wiki */
{
int i,j,k,x,y,z;
int row,col;
int v,w,progress;
int edge,swap;
int tip;
int donev,donew;
int a[20][20]; // hold input

printf("\nEnter N followed by NxN 0/1 matrix,G above, M below diagonal\n");
scanf(" %d",&N);
printf("\nN=%d",N);

N=N-1;    /* for spk compatability */

for (i=0; i<=N; i++)
   for (j=0; j<=N;j++)
        scanf("%d",&a[i][j]);


   for (i=0;i<=N;i++)
       {
        match[i]=i;
        shrunk[i]=0;
       }

   for (i=0; i<= N-1; i++)   // allows a matching to be input 
       for (j=i+1; j<=N;j++)
        {
        A[i][j]=a[i][j];
        if(a[j][i]==1)    // matching edge: j > i
         { match[i]=j;
           match[j]=i;
           matched++;
           matched++;
         }
       }
printf("\n Adjacency matrix");
for (i=0; i<=N; i++)
 {  printf(" \n");
    for (j=0; j<=N; j++)
     printf(" %d", A[i][j]);
 }

printf("\nmatch\n");
for (i=0; i<=N; i++)
   printf(" %d", match[i]);
printf("\ninitially %d vertices are matched",matched);

// main iteration of blossom 

blossom=0;
progress=1;
while (progress)  // Look for either an alt. path or blossom
{
    for(i=0;i<=N;i++)
       {
          odd[i]=0;
          marked[i]=0;
          if (shrunk[i])
              marked[i]=1;  //shrunk vertex
          else   
             {
             parent[i]=i;
             F[i]= ( match[i]==i);  //unmatched and alive
             printf("\n i=%d F=%d match=%d",i,F[i],match[i]);
             }
       }


   progress=0;
   v=0;
   donev=0;
   while (!progress && !donev ) //unexplored vertex
      {
         if (!marked[v] && !odd[v] && F[v]) // look for edges from v
          {
           printf("\nexplore from v=%d",v);
           marked[v]=1; // mark v
           w=0;
           donew=0;
           while (!progress && !donew )
             {
              if (v != w && !shrunk[w])
                   {
                     if (v<w)
                      edge=A[v][w];
                   else
                      edge=A[w][v];
               if ( edge && !odd[w] )  // consider all edges, marked or not
                   {
                    if (!F[w]) // w not in F
                       {
                        x=match[w];
                        parent[w]=v;
                        parent[x]=w;
                        F[w]=1;
                        F[x]=1;
                        odd[w]=1;
                        odd[x]=0;
                       }
                    else            // w in F
                       {
                            i=v;     // compute roots of subtrees
                            while (i != parent[i])
                                i=parent[i];
                            k=w;
                           while (k != parent[k])
                               k=parent[k];
                            if(i != k)  //path
                               {
                                printf("\n");
                                printpath(w,v);
                                matched=matched+2;
                                if(matched == N+1)
				    printf("\nperfect matching found!\n\n");
                                else
				    printf(" \n %d vertices matched\n",matched);
                                return 1;  
                               }
                            else           // shrink blossom
                               {
                                  x=parent[v];
                                  parent[v]=w;    // reverse vw
                                  while (v != x) //reverse edges from v to root
                                  {
                                      y=parent[x];  // y is temporary here
                                      parent[x]=v;  // reverse vx
                                      v=x;          // advance pointers
                                      x=y;
                                   }  // end reverse edges
                                      v=match[w];
                                  printf("\nblossom:t=%d",w);
                                  tip=0;

                                  while (v != w ) //shrink cycle to w
                                  {
                                   shrunk[v]=1;
                                   printf(" %d",v);
                                   if (!tip)
                                     if(!odd[v] && parent[v] != match[v]) //v is tip
                                       {
                                        tip=1;
                                        if (match[v]==v)
                                           match[w]=w;
                                        else
                                           match[w]=match[v];
                                       }
                                   j=0;    
                                   swap=0;
                                   col=w;
                                   while (j < v )     // shrink v to w  
                                       {
                                        if( w==j )
                                               {
                                                swap=1;
                                                row=w;
                                               }
                                         else
                                               {
                                                if (swap)
                                                  col=j;
                                                else
                                                  row=j;
                                                 A[row][col]= A[row][col] || A[j][v];
                                               }
                                           j++;
                                         }
                                                   //note: j=v, no need to reset swap 
                                   while (j != N)
                                     {
                                      j++;          // increment here to skip j=v
                                      if( w==j )
                                               {
                                                swap=1;
                                                row=w;
                                               }
                                      else
                                              {
                                               if (swap)
                                                 col=j;
                                               else
                                                 row=j;
                                                A[row][col]= A[row][col] || A[v][j];
                                               }
                                       }
                                   v=parent[v];    //get next node on cycle
                                  }  // end shrink cycle
                              }         //if path else blossom
                        progress=1;       // this iteration is over with progress
                        }      // w in F
                    }          //consider all edges vw
                   }           // if v != w
             if (w==N)
                 donew=1;
             else
                 w++;
                 }             // while w
            }                  // unexplored edge 
        if (v == N)
            donev=1;
        else
            v++;
         }                     //while v unexplored vertex
}  //Find either an alt. path or blossom or finished
                 
printf("\n Final data:");
printdata();
printf("\nno augmenting path found\n\n");
return 0;  
}

void printpath(int w, int v) // path from w,v, to root[v]
{
int i;
 printf("\nalternating path: %d %d",w,v);
 match[w]=v;
 match[v]=w;
 while (v != parent[v])
 {
     i=0;// i will be parent[v]
     while (A[i][v] != 0 ||  A[v][i] != 1)  
          i++;
     if(odd[v])
       { match[v]=i;
         match[i]=v;
       }
     v=i;
     printf(" %d",v);
  }  // end while
 if(w != parent[w])
   printf("\nalternating path: %d",w);
 while (w != parent[w])
 {
     i=0;// i will be parent[w]
     while (A[i][w] != 0 ||  A[w][i] != 1)  
          i++;
     if(odd[w])
       { match[w]=i;
         match[i]=w;
       }

     w=i;
     printf(" %d",w);
  }  // end while
}

void printblossom(int w, int v) 
{
int i,j;
 printf("\nreversing: %d %d",w,v);
 A[w][v]=1-A[w][v];  //reverse tree edge
 A[v][w]=1-A[v][w];  
 while (v != parent[v]) //reverse edges from w to root
 {
     i=0;// i will be parent[v]
     while (A[i][v] != 0 ||  A[v][i] != 1)
          i++;
     A[i][v]=1-A[i][v];  //reverse tree edge
     A[v][i]=1-A[v][i];  
     v=i;
     printf(" %d",v);
  }  // end reverse edges

     v=match[w];
 printf("\nblossom:tip=%d",w);
 while (v != w ) //print cycle
 {
     i=0;//     i will be parent[v]
     while (A[i][v] != 0 ||  A[v][i] != 1) 
          i++;
     printf(" %d",v);
     v=i;
  }  // end print cycle  

}

void printdata()
{
int i,j;

printf("\nodd\n");
for (i=0; i<=N; i++)
   printf(" %d", odd[i]);

printf("\nshrunk\n");
for (i=0; i<=N; i++)
   printf(" %d", shrunk[i]);
printf(" \n");

printf("marked\n");
for (i=0; i<=N; i++)
   printf(" %d", marked[i]);
printf(" \n");

printf("F\n");
for (i=0; i<=N; i++)
   printf(" %d", F[i]);
printf(" \n");

printf("parent\n");
for (i=0; i<=N; i++)
   printf(" %d", parent[i]);
printf(" \n");



fflush(stdout);
}
